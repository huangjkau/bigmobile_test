import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux'
import { addCount, delCount} from "./store/index.redux";

const mapStatetoProps = (state)=>{
    return {num: state.counter}
}
const actionCreators = { addCount, delCount};
@connect(mapStatetoProps, actionCreators)
class App extends Component {
    render() {
        // const store = this.props.store;
        // const num = store.getState();
        const num = this.props.num;
        const delCount = this.props.delCount;
        const addCount = this.props.addCount;
        return (
            <div>
                <h1>current {num}</h1>
                {/*<Button onClick={()=>store.dispatch(delCount())}>test</Button>*/}
                {/*<Button onClick={()=>store.dispatch(this.props.addCount())} bsStyle="success">Success</Button>*/}
                {/*<Button onClick={()=>store.dispatch(this.props.addAsyn())} bsStyle="success">Success</Button>*/}
                <Button onClick={delCount}>test</Button>
                <Button onClick={addCount} bsStyle="success">Success</Button>
            </div>
        );
    }
}

export default App;
