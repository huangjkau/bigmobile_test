
import axios from 'axios'
// import {userData} from "./auth.redux";
const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
const LOGOUT = 'LOGOUT'
const ERROR_MSG = 'ERROR_MSG'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const USERDATA = 'USERDATA';

const initState={
    isAuth:false,
    msg:'',
    Name:'',
    Role:''
}

export function user(state=initState, action) {
    switch(action.type){
        case USERDATA:
            return {...state, ...action.payload, isAuth:true,msg:''};
        case REGISTER_SUCCESS:
            return {...state, isAuth:true,msg:''};
        case LOGOUT:
            return {initState,msg:''}
        case LOGIN_SUCCESS:
            return {...state, isAuth:true, ...action.payload, msg:''}
        case ERROR_MSG:
            return {...state, isAuth:false, msg:action.msg}
        default:
            return state
    }
}

function userData(data) {
    return {type:USERDATA, payload:data};
}

function loginSuccess(data) {
    return {type:LOGIN_SUCCESS, payload:data}
}

function createSuccess() {
    return {type:REGISTER_SUCCESS}
}


function errorMsg(msg) {
    return {msg, type:ERROR_MSG}
}

export function logout() {
    return {type:LOGOUT}
}

export function getUserData() {
    return dispatch=>{
        // console.log(axios);
        // axios.get('/users/login').then(res=>{
        //     if (res.status === 200){
        //         console.log('####');
        //     }
        //     console.log(res)
        // })
        axios.get('/users/info').then(res=>{
            if (res.status === 200) {
                console.log(res.data)
                if (res.data.error){
                    console.log(res.data.error)
                    //invalid
                } else {
                    // console.log(this.props.history)
                    dispatch(userData(res.data.data))
                }
            }
        })
    }
}

export function userLogin(username, password) {
    //TODO validation
    return dispatch=>{
        axios.post('/users/login', {username, password})
            .then(res=>{
                if (res.status == 200 && res.data.data) {
                    //success
                    console.log(res.data.data)
                    dispatch(loginSuccess(res.data.data))
                    console.log('req success')
                } else {
                    dispatch(errorMsg(res.data.error))
                }
            })
    }
}

export function userCreate({username, password, role, gender, address, email, linkedin, birthday, phone}) {
    if (username) {
        return errorMsg('username should not be empty')
    }
    return dispatch=>{
        axios.post('/users/create', {username, password, role, gender, address, email, linkedin, birthday, phone})
        .then(res=>{
            if (res.status == 200) {
                //success
                dispatch(createSuccess())
                console.log('req success')
            } else {
                console.log('req err')
                dispatch(errorMsg(res.data.error))
            }
        })
    }

}