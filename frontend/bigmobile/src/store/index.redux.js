import {createStore} from 'redux'

const ADD_COUNT = 'add';
const DEL_COUNT = 'del';


export function counter(state=10, action) {
    switch (action.type) {
        case ADD_COUNT:
            return state + 1;
        case DEL_COUNT:
            return state - 1;
        default:
            return state;
    }
}

export function addCount() {
    return {type:ADD_COUNT}
}
export function delCount() {
    return {type:DEL_COUNT}
}



