import axios from "axios";

const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const USERDATA = 'USERDATA';


export function auth(state={isAuth : false, user: 'Bob'}, action) {
    switch (action.type) {
        case LOGIN:
            return {...state, isAuth : true};
        case LOGOUT:
            return {...state, isAuth : false};
        case USERDATA:
            return {...state, ...action.payload};
        default:
            return state
    }
}

// export function getUserData() {
//     return dispatch=>{
//         console.log(axios);
//         axios.get('/users/login').then(res=>{
//             if (res.status === 200){
//                 console.log('####');
//                 dispatch(userData(res.data))
//             }
//             console.log(res)
//         })
//     }
// }


// export function userData(data) {
//     return {type:USERDATA, payload:data};
// }

export function login() {
    return {type:LOGIN};
}

export function logout() {
    return {type:LOGOUT};
}
