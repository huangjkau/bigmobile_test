import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux'
import thunk  from 'redux-thunk'
import { BrowserRouter, Route, Link, Redirect, Switch, HashRouter} from 'react-router-dom'
import Login from './components/login'
import Home from './components/homePage'
import Error from './components/Error'
import AuthRoute from './components/authRoute'
import Header from './components/header'
import 'bootstrap/dist/css/bootstrap.css';


import { counter } from './store/index.redux'
import reducers from './store/reducers'
const store = createStore(reducers,compose(applyMiddleware(thunk)));
console.log(store.getState());

ReactDOM.render(
    <Provider store={store} >
        <BrowserRouter>
            <div style={{backgroundColor:'#e5e8e6',minHeight:'100vh'}}>
                <Header/>
                <AuthRoute></AuthRoute>
            <Switch>
                <Route path='/' exact component={Home}></Route>
                <Route path='/login' exact component={Login}></Route>
                <Route path='/home' component={Home}></Route>
                <Redirect to='/home'></Redirect>
                <Route path='/:unknow' component={Error}></Route>
            </Switch>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
