import React, { Component } from 'react';
import axios from 'axios'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { getUserData } from "../store/user";

const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = { getUserData } ;

//get cookie from server
@withRouter
@connect(mapStatetoProps, actionCreators)
class AuthRoute extends React.Component{

    componentDidMount(){
        // const publicList = ['/home/adduser']
        // const pathname = this.props.location.pathname;
        // console.log('path:' ,pathname)
        // if (publicList.indexOf(pathname))
        // axios.get('/users/info').then(res=>{
        //     if (res.status === 200) {
        //         console.log(res.data)
        //         if (res.data.error){
        //             console.log(res.data.error)
        //             //invalid
        //         } else {
        //             console.log(this.props.history)
        //             console.log(res.data.data)
        //         }
        //     }
        // })
        this.props.getUserData()
    }
    render(){
        return (<h1>auth page</h1>)
    }
}

export default AuthRoute