import React, { Component } from 'react';
import axios from 'axios'
import connect from "react-redux/es/connect/connect";
import '../../src/css/style.css'


const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = {} ;
@connect(mapStatetoProps, actionCreators)
class AllUsers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user_data:[],
            selected_data:[],
            search_user:'',
            search_state:true,
            selected_gender:'',
            selected_role:'',
            active: -1,
            username: '',
            password: '',
            confirmed_password: '',
            role: '',
            gender: '',
            address: '',
            email: '',
            linkedin: '',
            birthday: '',
            phone:'',
        }
        // this.archive = this.archive.bind(this)
        this.search = this.search.bind(this)
    }
    componentDidMount(){
        axios.get('/users').then(res=>{
            console.log(res.data.data)
            this.setState({
                user_data: res.data.data,
                selected_data: res.data.data
            })
            console.log(this.state)
            })
    }
    search(){
        console.log(this.state.search_user)
        let user_name = this.state.search_user
        let users = this.state.user_data.filter(v=>v.Name == user_name || !this.state.search_state)
        this.setState({
            selected_data: users,
            search_state:!this.state.search_state
        })

    }

    archive(id){
        axios.put('/users/archive',{id}).then(res=>{
            console.log(res.data.data)
            console.log(this.state)
            let user_data = this.state.user_data
            console.log(user_data)
            user_data = user_data.filter((v)=>v.id != id)
            console.log(user_data)
            this.setState({
                user_data:user_data,
                selected_data:user_data
            })
        })
    }
    edit(id, index){
        console.log(id, index);
        this.setState({
            active:index
        })
    }

    cancel(){
        console.log('calcel')
        this.setState({
            active:-1
        })
    }

    update(id){
        console.log(this.state)
        const username = this.state.username
        const password = this.state.password
        const role = this.state.role
        const gender = this.state.gender
        const address = this.state.address
        const email = this.state.email
        const linkedin = this.state.linkedin
        const birthday = this.state.birthday
        const phone = this.state.phone
        console.log(id)
        axios.put('/users/update', {id, username, password, role, gender, address, email, linkedin, birthday, phone})
            .then(res=>{
                if (res.status == 200) {
                    //success
                    console.log(res.data);
                    alert('crete user successful')
                    this.setState({active:-1})
                    //update data
                    axios.get('/users').then(res=>{
                        console.log(res.data.data)
                        this.setState({
                            user_data: res.data.data,
                            selected_data: res.data.data
                        })
                        console.log(this.state)
                    })
                } else {
                    //error
                    console.log(this.props)
                }
            }).catch(e=>{
            console.log('req error')
            console.log(e)
        })
    }

    handleChange(key, e){
        console.log(key, e.target.value)
        if(key === 'selected_gender'){
            let gender = e.target.value;
            let role = this.state.selected_role
            console.log(this.state.selected_data)
            let users = this.state.user_data.filter(v=>(v.Gender == gender || gender == '')&&(v.Role == role || role == ''))
            console.log(users)
            this.setState({
                selected_data:users
            })
            console.log(this.state)
        }
        if(key === 'selected_role'){
            let role = e.target.value;
            let gender = this.state.selected_gender
            let users = this.state.user_data.filter(v=>(v.Role == role || role == '')&&(v.Gender == gender || gender == ''))
            this.setState({
                selected_data:users
            })
        }
        this.setState({
            [key]:e.target.value
        })
    }

    render(){
        // const admin_op = (<div className='card-footer'>
        //     <div className='row justify-content-around'>
        //         <button className='btn btn-primary w-25'>Edit</button>
        //         <button className='btn btn-danger w-25' onClick={this.archive(v.id)}>Archive</button>
        //     </div>
        // </div>)

        console.log(this.props.Role);
        console.log(this.props.Role === 'Admin');
        return(
            <div className='container text-center' style={{marginTop:'100px'}}>
                {/*<h1>Users info</h1>*/}
                <div className="search_box row justify-content-center align-items-center">
                    <div className="row align-items-center"
                         style={{position: 'relative', width: '97%', height: '50px',background: 'white', borderRadius: '50px'}}>
                        <div className="col-2 text-center p-0 h-100" style={{lineHeight: '50px', textTransform: 'uppercase', fontFamily: 'Montserrat', borderRight: '1px solid darkgray'}}>Search
                        </div>
                        <div className='col-2' style={{borderRight: '1px solid darkgray'}}>
                            <div className='row align-items-center'>
                                <div className='col-4'>
                                    <div style={{fontFamily: 'Montserrat', lineHeight: '50px', fontSize:'12px'}}>Gender</div>
                                </div>
                                <div className='col-8'>
                                    <select className="custom-select" onChange={v=>this.handleChange('selected_gender',v)} required>
                                        <option value="">All</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Femal</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className='col-2' style={{borderRight: '1px solid darkgray'}}>
                            <div className='row align-items-center'>
                                <div className='col-4'>
                                    <div style={{fontFamily: 'Montserrat', lineHeight: '50px', fontSize:'12px'}}>Role</div>
                                </div>
                                <div className='col-8'>
                                    <select className="custom-select" onChange={v=>this.handleChange('selected_role',v)} required>
                                        <option value="">All</option>
                                        <option value="User">User</option>
                                        <option value="Admin">Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="input-group col-4 h-100 p-0 ">
                            <div className="row align-items-center ml-5">
                                <input name="email" type="text" id="search_input" className="form-control h-50" placeholder="Alice" onChange={(v)=>this.handleChange('search_user', v)}  autoFocus="" style={{outline: 'none', fontSize: '30px', textAlign: 'center',lineHeight:'70px'}}/>
                            </div>
                        </div>
                        <div className="col-2 text-right p-0">
                            {this.state.search_state?
                                <button className="search_form_button" onClick={this.search}>Search</button>:
                                <button className="search_form_button" onClick={this.search}>Cancel</button>

                            }
                        </div>
                    </div>
                </div>
                <div className='row mt-5'>
                    {this.state.selected_data.map((v, index)=>(
                        <div className='card mr-4 mb-5' key={v.id} style={{width:'350px'}}>
                            {/*<div className='card' key={v.id} style={{width:'350px'}}>*/}
                            <div className="card-header text-white" style={{backgroundColor:'#304c70'}}>
                                <h4>{v.Name}</h4>
                            </div>
                            <div className='card-body text-left' >
                                {this.state.active != index?(
                                    <div>
                                        <h4 className='mb-5 mt-5'>Birthday: {v.Date_of_birth.split('T')[0]}</h4>
                                        <hr/>
                                        <h4 className='mb-5'>Gender: {v.Gender}</h4>
                                        <h4 className='mb-5'>Address: {v.Address}</h4>
                                        <h4 className='mb-5'>Email_address: {v.Email_address}</h4>
                                        <h4 className='mb-5'>Phone_number: {v.Phone_number}</h4>
                                        <h4 className='mb-5'>Linkedin_profile: {v.Linkedin_profile}</h4>
                                        <h4 className='mb-5'>Phone_number: {v.Phone_number}</h4>
                                    </div>):
                                    (<div className='w-100 text-center'>
                                        <label>Name</label>
                                        <input type="text" className="form-control" onChange={v=>this.handleChange('username',v)} defaultValue={v.Name} placeholder="Nick" required/>
                                        <label>Role</label>
                                        <select className="custom-select" onChange={v=>this.handleChange('role',v)}>
                                            <option value="">Choose...</option>
                                            <option value="Admin">Admin</option>
                                            <option value="User">User</option>
                                        </select>
                                        <label>Date of birth</label>
                                        <input type="date" className="form-control" onChange={v=>this.handleChange('birthday',v)} defaultValue={v.Date_of_birth} required/>
                                        <label>Password</label>
                                        <input type="password" className="form-control" onChange={v=>this.handleChange('password',v)}required/>
                                        <label>Confirm password</label>
                                        <input type="password" className="form-control" onChange={v=>this.handleChange('confirmed_password',v)}required/>
                                        <label>Email address</label>
                                        <input type="text" className="form-control" onChange={v=>this.handleChange('email',v)} defaultValue={v.Email_address} placeholder="user@gmail.com" required/>
                                        <label>Address</label>
                                        <input type="text" className="form-control" onChange={v=>this.handleChange('address',v)} defaultValue={v.Address} placeholder="Sydney..." required/>
                                        <label>Linkedin profile</label>
                                        <input type="text" className="form-control" onChange={v=>this.handleChange('linkedin',v)}placeholder="user@gmail.com" defaultValue={v.Linkedin_profile} required/>
                                        <label>Phone</label>
                                        <input type="text" className="form-control" onChange={v=>this.handleChange('phone',v)} defaultValue={v.Phone_number} placeholder="0414999999" required/>

                                    </div>)}
                            </div>
                            {this.props.Role === 'Admin'?(<div className='card-footer'>
                                <div className='row justify-content-around'>
                                    {this.state.active != index?
                                        (
                                            <div className='w-100'>
                                                <button className='btn btn-primary w-25 mr-5' style={{backgroundColor:'#51b995'}} onClick={()=>this.edit(v.id, index)}>Edit</button>
                                                <button className='btn btn-danger w-25' style={{backgroundColor:'#5c7c92'}}  onClick={()=>this.archive(v.id)}>Archive</button>
                                            </div>
                                        ):(
                                            <div className='w-100'>
                                                <button className='btn btn-success w-25 mr-5' style={{backgroundColor:'#51b995'}} onClick={()=>this.update(v.id)}>Save</button>
                                                <button className='btn btn-danger w-25' style={{backgroundColor:'#5c7c92'}}  onClick={()=>this.cancel()}>Cancel</button>
                                            </div>
                                        )

                                    }
                                </div>
                            </div>):null}
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}



export default AllUsers