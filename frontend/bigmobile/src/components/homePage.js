import React, { Component } from 'react';
import { BrowserRouter, Route, Link, Redirect, Switch} from 'react-router-dom'
import { connect } from 'react-redux'
import  AddUser from './addUser'
import  EditUser from './editUser'
import App from '../App';
import AllUser from './allUser'
import {addCount, delCount} from "../store/index.redux";
import { logout } from "../store/auth.redux";

const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = { logout } ;
@connect(mapStatetoProps, actionCreators)

class Home extends Component {
    constructor(props) {
        super(props)
    }

    render(){
        console.log('#############');
        console.log(this.props);
        const match = this.props.match
        console.log(match)
        const redirectToLogin = <Redirect to='/login'></Redirect>
        const app = (
            <div>
                {this.props.isAuth? <button onClick={this.props.logout}>Logout</button>:null}



                {/*<ul>*/}
                    {/*<li>*/}
                        {/*<Link to='/login'>login</Link>*/}
                    {/*</li>*/}
                    {/*<li>*/}
                        {/*<Link to={match.url}>Home</Link>*/}
                    {/*</li>*/}
                    {/*<li>*/}
                        {/*<Link to={`${match.url}/adduser`}>AddUser</Link>*/}
                    {/*</li>*/}
                    {/*<li>*/}
                        {/*<Link to={`${match.url}/edituser`}>EditUser</Link>*/}
                    {/*</li>*/}
                {/*</ul>*/}
                <Route path={match.url} exact component={AllUser}></Route>
                <Route path={`${match.url}/adduser`}  component={AddUser}></Route>
                <Route path={`${match.url}/edituser`} component={EditUser}></Route>
            </div>
        )
        // return this.props.isAuth?app:redirectToLogin
        return app

    }
}

export default Home;