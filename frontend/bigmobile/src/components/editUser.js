import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";
import {  Redirect } from 'react-router-dom'
import axios from "axios";

const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = {} ;
@connect(mapStatetoProps, actionCreators)
class EditUser extends Component {
    constructor(props){
        super(props);
        this.update = this.update.bind(this);
        this.state = {
            username: this.props.Name,
            password: '',
            confirmed_password: '',
            gender: this.props.Gender,
            role: this.props.Role,
            address: this.props.Address,
            email: this.props.Email_address,
            linkedin: this.props.Linkedin_profile,
            birthday: this.props.Date_of_birth,
            phone:this.props.Phone_number,
            errorMsg: ''
        }
    }
    handleChange(key, e){
        console.log(key, e.target.value)
        this.setState({
            [key]:e.target.value
        })
    }

    update(){
        const username = this.state.username
        const password = this.state.password
        const role = this.state.role
        const gender = this.state.gender
        const address = this.state.address
        const email = this.state.email
        const linkedin = this.state.linkedin
        const birthday = this.state.birthday
        const phone = this.state.phone
        const id = this.props.id

        //TODO put into reducer change state
        console.log(id, username, password, role, gender, address, email, linkedin, birthday, phone)
        // this.props.userCreate({username, password, role, gender, address, email, linkedin, birthday, phone})
        axios.put('/users/update', {id, username, password, role, gender, address, email, linkedin, birthday, phone})
            .then(res=>{
                if (res.status == 200) {
                    //success
                    console.log(res.data);
                    alert('crete user successful')
                } else {
                    //error
                    console.log(this.props)
                }
            }).catch(e=>{
            console.log('req error')
            console.log(e)
        })
        console.log('finish')


    }
    render(){

        const redirectToLogin = <Redirect to='/login'></Redirect>
        console.log(this.props)
        return (
            <div className='container text-center' style={{marginTop:'100px'}}>
                {this.props.Role?null:redirectToLogin}
                <h1>edit user page</h1>
                <div className='container' style={{width:'70%'}}>
                    <div className='row justify-content-between'>
                        <div className='col-4'>
                            <label>Name</label>
                            <input type="text" className="form-control" onChange={v=>this.handleChange('username',v)} defaultValue={this.state.username} placeholder="Nick" required/>
                        </div>
                        <div className='col-4'>
                            <label>Role</label>
                            <select className="custom-select" onChange={v=>this.handleChange('role',v)} disabled={true}>
                                <option value="">Choose...</option>
                                <option value="Admin">Admin</option>
                                <option value="User">User</option>
                            </select>
                        </div>
                    </div>
                    <div className='row justify-content-between'>
                        <div className='col-4'>
                            <label>Date of birth</label>
                            <input type="date" className="form-control" onChange={v=>this.handleChange('birthday',v)} defaultValue={this.state.birthday} required/>
                        </div>
                        <div className='col-4'>
                            <label>Gender</label>
                            <select className="custom-select" onChange={v=>this.handleChange('gender',v)} defaultValue={this.state.gender} required>
                                <option value="">Choose...</option>
                                <option value="Male">Male</option>
                                <option value="Female">Femal</option>
                            </select>
                        </div>
                    </div>
                    <div className='row justify-content-between'>
                        <div className='col-4'>
                            <label>Password</label>
                            <input type="password" className="form-control" onChange={v=>this.handleChange('password',v)}required/>
                        </div>
                        <div className='col-4'>
                            <label>Confirm password</label>
                            <input type="password" className="form-control" onChange={v=>this.handleChange('confirmed_password',v)}required/>
                        </div>
                    </div>
                    <div className='row justify-content-between align-users-end'>
                        <div className='col-4'>
                            <label>Email address</label>
                            <input type="text" className="form-control" onChange={v=>this.handleChange('email',v)} defaultValue={this.state.email} placeholder="user@gmail.com" required/>
                        </div>
                        <div className="col-4">
                            <label>Address</label>
                            <input type="text" className="form-control" onChange={v=>this.handleChange('address',v)} defaultValue={this.state.address} placeholder="Sydney..." required/>
                        </div>
                    </div>
                    <div className='row justify-content-between align-users-end'>
                        <div className='col-4'>
                            <label>Linkedin profile</label>
                            <input type="text" className="form-control" onChange={v=>this.handleChange('linkedin',v)}placeholder="user@gmail.com" defaultValue={this.state.linkedin} required/>
                        </div>
                        <div className="col-4">
                            <label>Phone</label>
                            <input type="text" className="form-control" onChange={v=>this.handleChange('phone',v)} defaultValue={this.state.phone} placeholder="0414999999" required/>
                        </div>
                    </div>
                    <div className='row justify-content-center mt-5'>
                        <button className='btn btn-success w-75' style={{backgroundColor:'#5c7c92', borderRadius:'15px'}} onClick={this.update}>Update</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditUser;