import React, { Component } from 'react';
import { connect } from 'react-redux'
import {login, getUserData} from "../store/auth.redux";
import { Redirect} from 'react-router-dom'
import { Button, Row, Col } from 'react-bootstrap';
import { userLogin } from "../store/user";
import axios from 'axios'

const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = { userLogin };
@connect(mapStatetoProps, actionCreators)
class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.userLogin = this.userLogin.bind(this);
    }
    handleChange(key, e) {
        console.log(key, e.target.value)
        this.setState({
            [key]: e.target.value
        })
    }

    userLogin(){
        this.props.userLogin(this.state.username, this.state.password)
        console.log(this.state);
    }
    componentDidMount(){
        // console.log(axios);
        // axios.get('/users/login').then(res=>{
        //     console.log(res)
        // })
        // this.props.getUserData()
    }
    render(){
        console.log(this.props)
        return (
            <div>
                {this.props.isAuth?<Redirect to='/home'></Redirect>:null}
                <div className='row justify-content-center' style={{marginTop:'200px'}}>
                    <div className='col-lg-3'>
                        {/*<h1 className="h3 mb-3 font-weight-normal">Please login</h1>*/}
                        <img className='w-100 h-50 text-center' src="https://mumbrella.com.au/wp-content/uploads/2017/10/big-mobile-logo.png" alt=""/>
                        {this.props.msg?<h6 style={{color:'red'}}>{this.props.msg}</h6>:null}
                        <label htmlFor="inputEmail" className="sr-only">Username</label>
                        <input  type='text' className="form-control mb-4" placeholder="Username" onChange={v=>this.handleChange('username',v)} required autoFocus/>
                        <label  className="sr-only">Password</label>
                        <input type="password"  className="form-control mb-4" placeholder="Password" onChange={v=>this.handleChange('password',v)} required/>
                        {/*<div className="checkbox mb-3">*/}
                            {/*<label>*/}
                                {/*<input type="checkbox" value="remember-me"/>*/}
                            {/*</label>*/}
                        {/*</div>*/}
                        <button className="btn btn-lg btn-primary btn-block" style={{backgroundColor:'#5c7c92', borderRadius:'15px'}} onClick={this.userLogin}>Login</button>
                        {/*<p className="mt-5 mb-3 text-muted">&copy; 2017-2018</p>*/}
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;