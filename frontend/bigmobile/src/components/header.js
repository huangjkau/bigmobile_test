import React, { Component } from 'react';
import { BrowserRouter, Route, Link, Redirect, Switch} from 'react-router-dom'
import { connect } from 'react-redux'
import {logout} from "../store/user";
import '../../src/css/style.css'


const mapStatetoProps = (state)=>{
    return state.user
}
const actionCreators = { logout } ;
@connect(mapStatetoProps, actionCreators)
class Header extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        const admin_create = (
            <Link active-class="active" to="/home/adduser">
                <div className="navbar-brand text-right w-50" style={{marginLeft: '70px',lineHeight:'40px'}}>
                    {/*<img src="../assets/images/logo.png" alt="" style="height: 50px">*/}
                    Create
                </div>
            </Link>
        )

        return(
            <nav className="navbar navbar-expand-md fixed-top navbar-dark p-2" style={{backgroundColor:'#5c7c92'}}>
                    <div className='row w-100 justify-content-start'>
                    <div className='col-2'>
                        <Link active-class="active" to="/home">
                            <div className="navbar-brand text-center w-75" style={{lineHeight:'40px'}}>
                                {/*<img src="../assets/images/logo.png" alt="" style="height: 50px">*/}
                                Home
                            </div>
                        </Link>

                    </div>
                    {this.props.isAuth?
                        (
                            <div className='col-10 text-right w-100'>


                                {this.props.Role ==='Admin'?admin_create:null}
                                <Link active-class="active" to="/home/edituser">
                                    <div className={this.props.Role ==='Admin'?"navbar-brand text-right ":"navbar-brand text-right header_user"} style={{marginLeft: '30px',lineHeight:'40px'}}>
                                        {/*<img src="../assets/images/logo.png" alt="" style="height: 50px">*/}
                                        Edit
                                    </div>
                                </Link>
                                <div className="navbar-brand text-right" style={{marginLeft: '30px',lineHeight:'40px',cursor:'pointer'}} onClick={this.props.logout}>
                                    {/*<img src="../assets/images/logo.png" alt="" style="height: 50px">*/}
                                    Logout
                                </div>
                                <div className='navbar-brand' style={{marginLeft: '30px',lineHeight:'40px',cursor:'pointer', }} >Hi {this.props.Name}!</div>
                            </div>
                        ):(
                            <div className='col-9'>
                                <Link active-class="active" to="/login">
                                    <div className="navbar-brand text-right w-100" style={{lineHeight:'40px'}}>
                                        {/*<img src="../assets/images/logo.png" alt="" style="height: 50px">*/}
                                        Login
                                    </div>
                                </Link>

                            </div>
                        )}
                    </div>
                </nav>
        )
    }
}

export default Header