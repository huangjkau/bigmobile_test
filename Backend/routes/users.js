var express = require('express');
var router = express.Router();
const userController = require('../controllers/userController');

//Get all user's info
router.get('/', function(req, res, next) {
    userController.get_users_info(res)
})

//User login
router.post('/login', function(req, res, next) {
    userController.user_login(req, res)

    // res.json({ succ: 'success' })
})

//Create user
router.post('/create', function(req, res, next) {
    userController.create_user(req, res)
    // res.json({ succ: 'success' })
})


//Create user
router.put('/update', function(req, res, next) {
    userController.update_user(req, res)
})

//Archive
router.put('/archive', function(req, res, next) {
    userController.archive_user(req, res)
})

router.get('/info', function(req, res, next) {
    userController.user_info(req, res)
})

module.exports = router;