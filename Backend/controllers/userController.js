const models = require('../models');

module.exports = {
    user_login(req, res) {
        models.user.find({
            where: {
                Name: req.body.username,
                Password: req.body.password
            }
        }).then(data => {
            if (data) {
                //TODO cookie
                res.cookie('userId', data.dataValues.id)
                res.json({data: data})
            } else {
                res.json({error: 'invalid username or password'})
            }
        }).catch(error => {
            console.log(error)
        })

    },
    get_users_info(res) {
        //TODO add offset and limit
        models.user.findAll({
            where: {Archive:false}
        })
            .then(data => {
                res.json({data: data})
            }).catch(error => {
            console.log(error)
        })
    },

    create_user(req, res) {

        //TODO validation and password encryption
        models.user.create({
            Name: req.body.username,
            Password: req.body.password,
            Role: req.body.role,
            Gender: req.body.gender,
            Date_of_birth: req.body.birthday,
            Address: req.body.address,
            Email_address: req.body.email,
            Phone_number: req.body.phone,
            Linkedin_profile: req.body.linkedin,
        }).then(function (value) {
            if (value) {
                console.log('success')
                res.json({data: 'success'});
            }
        }).catch(function (reason) {
            // console.log(reason);
            res.json({error: reason});
        });
    },
    update_user(req, res) {
        //TODO validation and password encryption

        let update_info = {}
        if(req.body.username) update_info.Name = req.body.username;
        if(req.body.password) update_info.Password = req.body.password;
        if(req.body.role) update_info.Role = req.body.role;
        if(req.body.gender) update_info.Gender = req.body.gender;
        if(req.body.birthday) update_info.Date_of_birth = req.body.birthday;
        if(req.body.address) update_info.Address = req.body.address;
        if(req.body.email) update_info.Email_address = req.body.email;
        if(req.body.phone) update_info.Phone_number = req.body.phone;
        if(req.body.linkedin) update_info.Linkedin_profile = req.body.linkedin;

        console.log(update_info, req.body.id)
        models.user.update(
            update_info,
            {where:{id:req.body.id}}

        ).then(function (value) {
            if (value) {
                console.log('success')
                res.json({data: 'success'});
            }
        }).catch(function (reason) {
            // console.log(reason);
            res.json({error: reason});
        });

    },
    archive_user(req, res){
        //TODO validation
        models.user.update({
                Archive: true,
            },
            {where:{id:req.body.id}}
        ).then(function (value) {
            if (value) {
                console.log('success')
                res.json({data: 'success'});
            }
        }).catch(function (reason) {
            // console.log(reason);
            res.json({error: reason});
        });
    },
    user_info(req, res){
        const {userId} = req.cookies
        if (!userId) {
            res.json({error:'No cookie available'})
        } else {
            models.user.find({
                where: {
                    id: userId
                }
            }).then(data => {
                console.log(data)
                if (data) {
                    res.json({data: data})
                    console.log('success')
                } else {
                    res.json({error: 'invalid cookie'})
                    console.log('cookie error')
                }
            }).catch(error => {
                console.log(error)
            })
        }
    }
}
