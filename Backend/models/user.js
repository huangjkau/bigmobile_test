module.exports = function (sequelize, dataType) {
    const User = sequelize.define('user', {
        Name: {
            type: dataType.STRING,
            allowNull: false
        },
        Password: {
            type: dataType.STRING,
            allowNull: false
        },
        Role: {
            type: dataType.ENUM('Admin', 'User'),
            allowNull: false
        },
        Gender: {
            type: dataType.ENUM('Male', 'Female'),
            allowNull: false
        },
        Date_of_birth: {
            type:dataType.DATE,
            allowNull: false
        },
        Address: {
            type: dataType.STRING,
            allowNull: false
        },
        Email_address: {
            type: dataType.STRING,
            allowNull: false
        },
        Phone_number: {
            type: dataType.INTEGER,
            allowNull: false
        },
        Linkedin_profile: {
            type: dataType.STRING
        },
        Archive: {
            type: dataType.BOOLEAN,
            defaultValue:false
        },
    }, {
        underscored: true
    });






    return User;
};
